package br.ufma.ecp.lads.smartparking

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationManager
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.provider.Settings



const val SHARED_PREFERENCES_FILE_KEY_TRANSITIONS = "SHARED_PREFERENCES_FILE_KEY_TRANSITIONS"
const val SHARED_PREFERENCES_KEY_TRANSITIONS = "SHARED_PREFERENCES_KEY_TRANSITIONS"

class LastLocationReceiver : BroadcastReceiver() {
    private lateinit var locationRequest: LocationRequest
    lateinit var mFusedLocationClient: FusedLocationProviderClient
    private val PERMISSION_ID = 42


    override fun onReceive(context: Context?, intent: Intent?) {


        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context!!)

       /* mFusedLocationClient.lastLocation.addOnCompleteListener(){ task ->
            var location: Location? = task.result
            if (location != null) {
                val transition = intent!!.getStringExtra("transition")
                val locationTransition = " ${location.latitude.toString()},${location.longitude.toString()}; ${transition}"
                Toast.makeText(context, locationTransition , Toast.LENGTH_LONG).show()
                ForegroundService.startService(context.applicationContext, locationTransition)
                saveTransition(locationTransition, context!!)

            }*/

            if (checkPermissions(context)){
                if (isLocationEnable(context)){
                    mFusedLocationClient.lastLocation.addOnCompleteListener(){ task ->
                        var location: Location? = task.result
                        if (location == null){
                            requestNewLocationData()
                        }else{
                            val transition = intent!!.getStringExtra("transition")
                            val locationTransition = " ${location.latitude.toString()},${location.longitude.toString()}; ${transition}"
                            Toast.makeText(context, locationTransition , Toast.LENGTH_LONG).show()
                            ForegroundService.startService(context.applicationContext, locationTransition)
                            saveTransition(locationTransition, context!!)
                        }
                    }
            }else{
                    Toast.makeText(context, "Turn on location", Toast.LENGTH_LONG).show()
                    context.startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))

                }
        }else{
                requestPermissions(context)
            }
    }


   /* private fun getLastLocation(context: Context) {
        if (checkPermissions(context)){
            if (isLocationEnable(context)){
                mFusedLocationClient.lastLocation.addOnCompleteListener(){ task ->
                    var location: Location? = task.result
                    if (location == null){
                        requestNewLocationData()
                    }else{
                        Toast.makeText()
                    }
                }
            }
        }
    }*/

    private fun requestNewLocationData(){
        var mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 2000
        mLocationRequest.fastestInterval = 1000
    }

    private fun isLocationEnable(context: Context): Boolean{
        val locationManager: LocationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun checkPermissions(context: Context): Boolean{
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )== PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            )== PackageManager.PERMISSION_GRANTED )
        {
            return true
        }

        return false
    }

    private fun requestPermissions(context: Context) {
        ActivityCompat.requestPermissions(
            context as Activity,
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_BACKGROUND_LOCATION,
                Manifest.permission.ACTIVITY_RECOGNITION
            ),
            PERMISSION_ID
        )
    }

    fun onRequestPermissionResult(requestCode: Int, permissions: Array<String>, grantResult: IntArray){
        if (requestCode == PERMISSION_ID){
            if ((grantResult.isNotEmpty() && grantResult[0] == PackageManager.PERMISSION_GRANTED)){

            }
        }
    }

    fun saveTransition(locationTransition: String, mContext:Context){

        val sharedPref = mContext?.getSharedPreferences(
            SHARED_PREFERENCES_FILE_KEY_TRANSITIONS, Context.MODE_PRIVATE)

        var previousTransitions = sharedPref.getString(SHARED_PREFERENCES_KEY_TRANSITIONS, "")

        with (sharedPref.edit()) {
            val transition =  previousTransitions  + locationTransition
            putString(SHARED_PREFERENCES_KEY_TRANSITIONS, transition)
            commit()
        }
    }

}