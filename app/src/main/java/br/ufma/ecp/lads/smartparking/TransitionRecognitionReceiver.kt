package br.ufma.ecp.lads.smartparking

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast
import com.google.android.gms.location.ActivityTransition
import com.google.android.gms.location.ActivityTransitionEvent
import com.google.android.gms.location.ActivityTransitionResult
import com.google.android.gms.location.DetectedActivity
import java.text.SimpleDateFormat
import java.util.*

class TransitionRecognitionReceiver : BroadcastReceiver() {


    lateinit var receiver:LastLocationReceiver

    init {
        receiver = LastLocationReceiver()
    }


    override fun onReceive(context: Context?, intent: Intent?) {

        Toast.makeText(context, "Receive", Toast.LENGTH_SHORT).show()

        if (ActivityTransitionResult.hasResult(intent)) {
            val result = ActivityTransitionResult.extractResult(intent)!!
            for (event in result.transitionEvents) {
                val transition =   createTranstionString(event)
                //receiver.saveTransition("(Nolocation): ${transition}", context!!) // caso de erro para pegar a localizacao
                //Toast.makeText(context, transition, Toast.LENGTH_LONG).show()
                val intent = Intent("br.ufma.ecp.lads.smartparking.LastLocationReceiver")
                intent.putExtra("transition", transition)
                context!!.sendBroadcast(intent)

            }
        }

    }



    fun createTranstionString(activity: ActivityTransitionEvent): String {
        val theActivity = toActivityString(activity.getActivityType())
        val transType = toTransitionType(activity.getTransitionType())

        return ( theActivity + " (" + transType + ")" + "   "
                + SimpleDateFormat("HH:mm:ss", Locale.getDefault())
            .format(Date()) +"\n\n")
    }


    fun toActivityString(activity: Int): String {
        when (activity) {
            DetectedActivity.STILL -> return "STILL"
            DetectedActivity.WALKING -> return "WALKING"
            DetectedActivity.ON_BICYCLE -> return "ON_BICYCLE"
            DetectedActivity.ON_FOOT -> return "ON_FOOT"
            DetectedActivity.IN_VEHICLE -> return "IN_VEHICLE"
            DetectedActivity.RUNNING -> return "RUNNING"
            else -> return "UNKNOWN"
        }
    }

    fun toTransitionType(transitionType: Int): String {
        when (transitionType) {
            ActivityTransition.ACTIVITY_TRANSITION_ENTER -> return "ENTER"
            ActivityTransition.ACTIVITY_TRANSITION_EXIT -> return "EXIT"
            else -> return "UNKNOWN"
        }
    }


}