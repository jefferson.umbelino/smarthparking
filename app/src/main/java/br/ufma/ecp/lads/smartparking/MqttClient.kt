package br.ufma.ecp.lads.smartparking



import android.content.Context
import android.util.Log
import org.eclipse.paho.android.service.MqttAndroidClient
import org.eclipse.paho.client.mqttv3.IMqttActionListener
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken
import org.eclipse.paho.client.mqttv3.IMqttToken
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended
import org.eclipse.paho.client.mqttv3.MqttClient
import org.eclipse.paho.client.mqttv3.MqttException
import org.eclipse.paho.client.mqttv3.MqttMessage
import org.eclipse.paho.client.mqttv3.MqttConnectOptions



class MqttClient(private val context: Context) {

    val client by lazy {
        val clientId = MqttClient.generateClientId()

        MqttAndroidClient(context, "tcp://soldier.cloudmqtt.com:12779",
            //MqttAndroidClient(context, "tcp://localhost:1883",
            clientId)
    }


    companion object {
        const val TAG = "MqttClient"
    }

    private val connectOptions: MqttConnectOptions by lazy {
        MqttConnectOptions().apply {
            isCleanSession = false
            isAutomaticReconnect = true
            userName = "dhaccsur"
            password = "Ax_tdnx5Do0P".toCharArray()

        }
    }


    fun connect() {

        try {
            val token = client.connect(connectOptions)
            Log.i("Connectando", "iniciando ... ")
            if (!client.isConnected) {
                Log.i("Connectando", "ainda nao ")
            }
            token.actionCallback = object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken)                        {
                    Log.i("Connection", "success ")

                }
                override fun onFailure(asyncActionToken: IMqttToken, exception: Throwable) {
                    //connectionStatus = false
                    Log.i("Connection", "failure")
                    // Give your callback on connection failure here
                    exception.printStackTrace()
                }
            }
        } catch (e: MqttException) {
            // Give your callback on connection failure here
            e.printStackTrace()
        }
    }



    fun publish(topic: String, data: String) {
        val encodedPayload : ByteArray
        try {
            Log.d(TAG, "$data published to $topic")
            encodedPayload = data.toByteArray(charset("UTF-8"))
            val message = MqttMessage(encodedPayload)
            message.qos = 2
            message.isRetained = false
            client.publish(topic, message)
        } catch (e: Exception) {
            // Give Callback on error here
        } catch (e: MqttException) {
            // Give Callback on error here
        }
    }


    fun subscribeTopic(topic: String, qos: Int = 0) {
        client.subscribe(topic, qos).actionCallback = object : IMqttActionListener {
            override fun onSuccess(asyncActionToken: IMqttToken) {
                Log.d(TAG, "Subscribed to $topic")
            }

            override fun onFailure(asyncActionToken: IMqttToken, exception: Throwable) {
                Log.d(TAG, "Failed to subscribe to $topic")
                exception.printStackTrace()
            }
        }
    }

    fun close() {
        client.apply {
            unregisterResources()
            close()
        }
    }
}