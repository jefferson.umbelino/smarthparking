package br.ufma.ecp.lads.smartparking

import android.Manifest
import android.annotation.TargetApi
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.LocationManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import kotlinx.android.synthetic.main.activity_main.*
import com.google.android.gms.location.Geofence



class MainActivity : AppCompatActivity() {

    lateinit var lastLocationReceiver: LastLocationReceiver
    //private val PERMISSION_ID = 42

    private lateinit var mTransitionRecognition: TransitionRecognition


    lateinit var geofencingClient: GeofencingClient

    private val runningQOrLater =
        android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q


    private val geofencePendingIntent: PendingIntent by lazy {
        val intent = Intent(this, GeofenceBroadcastReceiver::class.java)
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when calling
        // addGeofences() and removeGeofences().
        PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        lastLocationReceiver = LastLocationReceiver()
        mTransitionRecognition = TransitionRecognition(this)

        val filter = IntentFilter("br.ufma.ecp.lads.smartparking.LastLocationReceiver")
        registerReceiver(lastLocationReceiver, filter)


        geofencingClient = LocationServices.getGeofencingClient(this)

    }


    override fun onResume() {
        super.onResume()
        showTransitions()

    }

    override fun onPause() {
        super.onPause()

    }

    override fun onDestroy() {
        super.onDestroy()
    }

    /*private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_BACKGROUND_LOCATION,
                Manifest.permission.ACTIVITY_RECOGNITION
            ),
            PERMISSION_ID
        )
    }

    private fun isLocationEnable(): Boolean{
        var locationManager: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return  locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
    }*/


    fun startTracking(v: View) {

        mTransitionRecognition.startTracking()


        ForegroundService.startService(this, "Foreground Service is running...")


        //startService(intent)
        ContextCompat.startForegroundService(this, intent)
    }

    fun stopTracking(v: View) {


        ForegroundService.stopService(this)

        unregisterReceiver(lastLocationReceiver)
        mTransitionRecognition.stopTracking()
    }

    fun showTransitions() {
        val sharedPref = getSharedPreferences(
            SHARED_PREFERENCES_FILE_KEY_TRANSITIONS, Context.MODE_PRIVATE
        )

        var previousTransitions = sharedPref.getString(SHARED_PREFERENCES_KEY_TRANSITIONS, "")



        textView.text = previousTransitions
    }

    fun addGeofence(v: View) {

        val currentGeofenceData = GeofencingConstants.LANDMARK_DATA[0]

        Toast.makeText(
            this@MainActivity,
            "${currentGeofenceData.latLong.toString()}",
            Toast.LENGTH_LONG
        ).show()

        val geofence = Geofence.Builder()
            // Set the request ID, string to identify the geofence.
            .setRequestId(currentGeofenceData.id)
            // Set the circular region of this geofence.
            .setCircularRegion(
                currentGeofenceData.latLong.latitude,
                currentGeofenceData.latLong.longitude,
                GeofencingConstants.GEOFENCE_RADIUS_IN_METERS
            )
            // Set the expiration duration of the geofence. This geofence gets
            // automatically removed after this period of time.
            .setExpirationDuration(GeofencingConstants.GEOFENCE_EXPIRATION_IN_MILLISECONDS)
            // Set the transition types of interest. Alerts are only generated for these
            // transition. We track entry and exit transitions in this sample.
            //.setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
            .setNotificationResponsiveness(300)
            .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER or Geofence.GEOFENCE_TRANSITION_EXIT)
            .build()

        val geofencingRequest = GeofencingRequest.Builder()
            // The INITIAL_TRIGGER_ENTER flag indicates that geofencing service should trigger a
            // GEOFENCE_TRANSITION_ENTER notification when the geofence is added and if the device
            // is already inside that geofence.
            .setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)

            // Add the geofences to be monitored by geofencing service.
            .addGeofence(geofence)
            .build()

        // First, remove any existing geofences that use our pending intent
        geofencingClient.removeGeofences(geofencePendingIntent)?.run {
            // Regardless of success/failure of the removal, add the new geofence
            addOnCompleteListener {
                // Add the new geofence request with the new geofence
                geofencingClient.addGeofences(geofencingRequest, geofencePendingIntent)?.run {
                    addOnSuccessListener {
                        // Geofences added.
                        //Toast.makeText(this@HuntMainActivity, R.string.geofences_added,Toast.LENGTH_SHORT).show()
                        Log.e("Add Geofence", geofence.requestId)
                        Toast.makeText(this@MainActivity, "added geofence", Toast.LENGTH_LONG)
                            .show()
                        // Tell the viewmodel that we've reached the end of the game and
                        // activated the last "geofence" --- by removing the Geofence.
                        //viewModel.geofenceActivated()
                    }
                    addOnFailureListener {
                        // Failed to add geofences.
                        // Toast.makeText(this@HuntMainActivity, R.string.geofences_not_added, Toast.LENGTH_SHORT).show()
                        if ((it.message != null)) {
                            //   Log.w(TAG, it.message)
                        }
                    }
                }
            }
        }

    }


    fun removeGeofences(v: View) {

        geofencingClient.removeGeofences(geofencePendingIntent)?.run {
            addOnSuccessListener {
                // Geofences removed
                //Log.d(TAG, getString(R.string.geofences_removed))
                Toast.makeText(applicationContext, "removed geofence", Toast.LENGTH_SHORT).show()
            }
            addOnFailureListener {
                // Failed to remove geofences
                //Log.d(TAG, getString(R.string.geofences_not_removed))
            }
        }
    }

}
