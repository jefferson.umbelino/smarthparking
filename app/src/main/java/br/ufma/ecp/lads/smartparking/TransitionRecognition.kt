package br.ufma.ecp.lads.smartparking

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener

class TransitionRecognition (val mContext: Context) {


    private val TAG = TransitionRecognition::class.java!!.getSimpleName()

    lateinit var mPendingIntent: PendingIntent

    lateinit var request: ActivityTransitionRequest

    init {
        val transitions = ArrayList<ActivityTransition>()

        transitions.add(ActivityTransition.Builder()
            .setActivityType(DetectedActivity.STILL)
            .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
            .build())

        transitions.add(ActivityTransition.Builder()
            .setActivityType(DetectedActivity.STILL)
            .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
            .build())

        transitions.add(ActivityTransition.Builder()
            .setActivityType(DetectedActivity.WALKING)
            .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
            .build())

        transitions.add(ActivityTransition.Builder()
            .setActivityType(DetectedActivity.WALKING)
            .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
            .build())

        transitions.add(ActivityTransition.Builder()
            .setActivityType(DetectedActivity.IN_VEHICLE)
            .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
            .build())

        transitions.add(ActivityTransition.Builder()
            .setActivityType(DetectedActivity.IN_VEHICLE)
            .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
            .build())

        transitions.add(
            ActivityTransition.Builder()
            .setActivityType(DetectedActivity.ON_BICYCLE)
            .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
            .build())

        transitions.add(ActivityTransition.Builder()
            .setActivityType(DetectedActivity.ON_BICYCLE)
            .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
            .build())

        transitions.add(ActivityTransition.Builder()
            .setActivityType(DetectedActivity.RUNNING)
            .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_ENTER)
            .build())

        transitions.add(ActivityTransition.Builder()
            .setActivityType(DetectedActivity.RUNNING)
            .setActivityTransition(ActivityTransition.ACTIVITY_TRANSITION_EXIT)
            .build())

        request = ActivityTransitionRequest(transitions)

    }


    fun startTracking() {

        val activityRecognitionClient = ActivityRecognition.getClient(mContext)

        val intent = Intent(mContext, TransitionRecognitionReceiver::class.java)
        mPendingIntent = PendingIntent.getBroadcast(mContext, 0, intent, 0)

             val task = activityRecognitionClient.requestActivityTransitionUpdates(request, mPendingIntent)
        task.addOnSuccessListener(
            object : OnSuccessListener<Void> {
                override fun onSuccess(p0: Void?) {
                    Toast.makeText(mContext,"sucesso",Toast.LENGTH_LONG).show()
                }
            })

        task.addOnFailureListener(
            OnFailureListener { })
    }


    fun stopTracking() {
        val activityRecognitionClient = ActivityRecognition.getClient(mContext)

        if (mContext != null && mPendingIntent != null) {
            activityRecognitionClient.removeActivityTransitionUpdates(mPendingIntent)
                .addOnSuccessListener(OnSuccessListener<Void> {
                    mPendingIntent.cancel()
                })
                .addOnFailureListener(OnFailureListener { e -> Log.e(TAG, "Transitions could not be unregistered: $e") })
        }
    }


}